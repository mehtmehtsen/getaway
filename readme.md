# Kiosk Framework
Sass basiertes Starter Framework für Kirby. [http://oliverbrux.de]

### Kirby installieren
1. Projekt Ordner im Finder/Explorer anlegen
2. Projekt auf Bitbucket anlegen
3. Im Terminal:

		git clone —recursive https://github.com/getkirby/plainkit.git

4. Das locale Kirby Git repository mit dem Bitbucket Repository verbinden:
	
		git remote set-url origin https://obearone@bitbucket.org/imkiosk/kiosk-framework.git

### Bower integrieren
Über Bower können im folgenden Plugins wie slider etc installiert werden. 
Mit folgendem Befehl können die benötigten Plugins im Projekt angelegt werden:

		bower install --save jquery jeet.gs bourbon 

Anschließend nur noch mit `bower install` eine frische Kopie der Plugins installieren. 

mit `bower update` können Plugins dann bequem aktualisiert werden. 

### Gulp integrieren
Falls noch nicht gemacht, Gulp global installieren:

		npm install --global gulp


Gulp im Projekt-Ordner installieren:
		
		npm install --save-dev gulp

#### Benötigte Node Plugins installieren:

        npm install
liest die package.json aus und installiert alle aufgeführten Packages.

#### Gulps /gulp/config.js konfigurieren

##### Grundsätzliches
- Der Variable `url` die dev-URL zuweisen (zB. `http://kiosk.dev:8888` ). Die Variable wird von Browsersync und uncss benutzt.
- Für statische Seiten unter `browserSync` den entsprechenden Block ein- und den anderen auskommentieren.

##### uncss
- Unter `uncss` in der Variable `html` so viele Unterseiten anlegen, bis alle CSS-Elemente abgedeckt werden, die auf der Seite benutzt werden.
- Die Variable `ignore` kann mit Elementen gefüllt werden, die uncss unrechtmäßig aussortiert. Dabei beachten: die Ignore-Regel `".meinSelektor"` betrifft nicht nested Classes wie `".andererSelektor .meinSelektor"` , es muss explizit `".andererSelektor .meinSelektor"` in die Liste aufgenommen werden.

### Javascript-Concanetation durch gulp-include
In `assets/js/main/main.js` können .js-Dateien anhand der Syntax
`//= include relativerPfad/Datei.js`
inkludiert werden. In der Website muss dann nur noch `app/js/main.js` eingebunden werden.
Gulp überwacht alle .js-Dateien in `assets/js` (also auch in `assets/js/vendors`), nicht jedoch in `assets/bower_components`. Wird dort etwas aktualisiert, kann beispielsweise durch Hinzufügen und Löschen einer leeren Zeile in der `main.js` das erneute Konkatenieren ausgelöst werden.

### Generieren von Iconfonts
`gulp iconFont` generiert aus allen .svg-Dateien in `assets/icons` einen Iconfont nach `app/iconfonts` und eine `_iconfonts.sass` nach `assets/05_base`.
Die .svg-Dateien müssen folgendermaßen gespeichert werden:

Beware that your SVG icons must have a high enough height. 500 is a minimum. If you do not want to resize them, you can try to combine the fontHeight and the normalize option to get them in a correct size.

#### Inkscape
Degroup every shape (Ctrl+Shift+G), convert to paths (Ctrl+Shift+C) and merge them (Ctrl++). Then save you SVG, prefer 'simple SVG' file type.

#### Illustrator
Save your file as SVG with the following settings:
- SVG Profiles: SVG 1.1
- Fonts Type: SVG
- Fonts Subsetting: None
- Options Image Location: Embed
- Advanced Options
    - CSS Properties: Presentation Attributes
    - Decimal Places: 1
    - Encoding: UTF-8
    - Output fewer elements: check

#### Iconfonts in der Seite benutzen
Vorher `gulp iconFont` oder `gulp production` aufrufen.
`<p class="icon -paperplane">Iconfont Paperplane</p>`

### gulp production
Vor dem Hochladen auf den Server wird in der Konsole `gulp production` ausgeführt.
Das führt folgende Tasks aus:
    - `images` (Bilder aus `assets/images` werden optimiert und in `app/images` verschoben)
    - `iconFont` (s.o.)
    - `minifyCss` (minifiziert `app/main.css` - durch Änderungen an den .scss-Dateien wird sie neu generiert und muss dann neu minifiziert werden)
    - `uglifyJs` (Uglyfiziert und minifiziert `app/main.js`)