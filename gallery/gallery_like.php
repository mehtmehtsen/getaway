<?php
include("mysql_connect.php");

$ip = $_SERVER['REMOTE_ADDR'];

if ($_POST['id']) {
  $id = $_POST['id'];

  //Verify IP address in Voting_IP table
  $stmt_cnt = $conn->prepare(
    "SELECT * FROM likeips WHERE ip = ? AND image_id_fk = ?");
  $stmt_cnt->bind_param('ss', $ip, $id);
  $stmt_cnt->execute();
  //$stmt_cnt->bind_result($col0, $col1, $col2);
  $stmt_cnt->store_result();
  $stmt_cnt->fetch();

  $count = $stmt_cnt->num_rows;
  //error_log("HALLO MEHT, HIER!! " . $count);

  $stmt_cnt->close();

  if ($count == 0) {
    // Update Vote.
    $stmt_like = $conn->prepare("UPDATE gtabilder SET likes = likes+1 WHERE image_id = ?");
    $stmt_like->bind_param('i',
      $id
    );
    $stmt_like->execute();
    $stmt_like->close();

    // Insert IP address and Message Id in Voting_IP table.
    $stmt_ip = $conn->prepare("INSERT INTO likeips(image_id_fk, ip) VALUES (?, ?)");
    $stmt_ip->bind_param('is',
      $id,
      $ip
    );
    $stmt_ip->execute();
    $stmt_ip->close();

    echo "1";
  }
  else {
    echo "0";
  }
}
?>
