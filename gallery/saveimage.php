<?php
include("mysql_connect.php");

if (isset($GLOBALS["HTTP_RAW_POST_DATA"])) {
  //error_log("trying to save image");

  $imageData = $GLOBALS["HTTP_RAW_POST_DATA"];

  // Remove the headers (data:,) part.
  // A real application should use them according to needs such as to check image type
  $filteredData = substr($imageData, strpos($imageData, ",") + 1);

  $unencodedData = base64_decode($filteredData);

  // pfade und datum
  $path = 'imagedb/';
  $filename = uniqid() . '.png';
  $pathFilename = $path . $filename;
  $date = date("Y-m-d H:i:s");

  // datei schreiben
  $fp = fopen('../' . $pathFilename, 'wb');
  fwrite($fp, $unencodedData);
  fclose($fp);

  $thumbPathFilename = 'thumbsdb/' . $filename;
  createthumb('../' . $pathFilename, '../' . $thumbPathFilename, 400);

  $sql = "INSERT INTO gtabilder (image_path, thumb_path, submission_date, likes)VALUES ('$pathFilename', '$thumbPathFilename', '$date', 0)";

  if ($result = $conn->query($sql)) {
      //echo "New record created successfully";
  } else {
      //echo "Error: " . $sql . "<br>" . $conn->error;
  }

  $row = mysql_fetch_row($result);

  $thumbSize = getimagesize('../' .$thumbPathFilename);
  $sizehtml = $thumbSize[3];

  $returnData = array(
    $thumbPathFilename,
    $pathFilename,
    $thumbSize[0],
    $thumbSize[1],
    $thumbSize[3],
    $conn->insert_id // id bzw image_id der zuletzt inserteten zeile
  );
  echo json_encode($returnData);

  $conn->close();
}

// USAGE: createthumb('pics/apple.jpg', 'thumbs/tn_apple.jpg', 100);
function createthumb($img_name, $thumb_name, $new_w) {
	$system = explode(".", $img_name);
	//if (preg_match("/jpg|jpeg/",$system[1])){$src_img=imagecreatefromjpeg($img_name);}
	//if (preg_match("/png/", $system[1])) {
	$src_img = imagecreatefrompng($img_name);
	//}

	$old_w = imageSX($src_img);
	$old_h = imageSY($src_img);

	$thumb_w = $new_w;
	$thumb_h = round($old_h * ($new_w / $old_w));

	$dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
	imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_w, $old_h);

	imagepng($dst_img, $thumb_name);

	imagedestroy($dst_img);
	imagedestroy($src_img);
}


?>
