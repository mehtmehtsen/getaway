<?php
include("mysql_connect.php");

//$sql = "SELECT image_id, image_path, thumb_path, submission_date, likes FROM gtabilder";

$page = 0;
if ($_GET['page']) {
  $page = intval($_GET['page']);
}

$pageOffset = $page * 30;

$sql = "SELECT image_id, image_path, thumb_path, submission_date, likes FROM gtabilder ORDER BY likes DESC LIMIT 30 OFFSET $pageOffset";

//error_log("MIMIMIMIMIMIMIMIMIMImImimimIIM");
//error_log($page);

if ($result = $conn->query($sql)) {
  // output data of each row
  while($row = $result->fetch_row()) {
    $thumbSize = getimagesize("../" . $row[2]);
    $thumbSizeX = intval($thumbSize[0] / 2);
    $thumbSizeY = intval($thumbSize[1] / 2);

    $imgSize = getimagesize("../" . $row[1]);
    $imgSizeX = intval($imgSize[0]);
    $imgSizeY = intval($imgSize[1]);
    ?>

    <div class="grid-item">
      id:<?=$row[0]?><br>
      <a href='<?=$row[1]?>' data-size='<?=$imgSizeX?>x<?=$imgSizeY?>' id='<?=$row[0]?>' data-imgid='<?=$row[0]?>' class='imglink'><img src='<?=$row[2]?>' width='<?=$thumbSizeX?>' height='<?=$thumbSizeY?>'><br></a>
      <a href='javascript:;' class='link like hvr' id='<?=$row[0]?>'>
        <span class='heart'>
          <img src='app/images/like.svg' alt='LIKE' class='hvr0'>
          <img src='app/images/like-voted.svg' alt='LIKE' class='hvr1' style='display:none;'>
        </span>
        <span class='heart-voted' style='display:none;'>
          <img src='app/images/like-voted.svg' alt='LIKE'>
        </span>
        <span id='likes'><?=$row[4]?></span>
        <span id='nope' style='display:none;'>vote once!1</span>
      </a>
    </div>

    <?php
  }

  $result->close();
}
$conn->close();

//echo "mehtus!";
?>
