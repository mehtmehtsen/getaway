var gulp        = require('gulp');
var config      = require('../config').production;
var minifyCSS   = require('gulp-minify-css');
var size        = require('gulp-filesize');
var uncss       = require('gulp-uncss');
var configUncss = require('../config').uncss;

gulp.task('minifyCss', ['sass'], function() {
  return gulp.src(config.cssSrc)
    .pipe(uncss({
      html: configUncss.html,
      ignore: configUncss.ignore
    }))
    .pipe(minifyCSS({
    	keepBreaks:false,
    	keepSpecialComments: 0
    }))
    .pipe(gulp.dest(config.cssDest))
    .pipe(size());
})
