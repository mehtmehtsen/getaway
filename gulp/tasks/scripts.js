var gulp         = require('gulp');
var include      = require('gulp-include');
var browserSync  = require('browser-sync');
var conf         = require('../config').scripts;

gulp.task('scripts', function () {
   return gulp.src(conf.mainSrc)
   		.pipe(include())
      .pipe(gulp.dest(conf.dest))
      .pipe(browserSync.reload({stream:true}));
});
