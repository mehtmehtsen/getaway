var dest = "./app";
var src = "./assets";

var url = "http://kiosk.gta:8888"; // DEVELOPMENT URL

module.exports = {
  browserSync: {
    // statische seite
    conf: {
      proxy: url,
      files: ['**/*.php', 'assets/js/**/*.js'],
      watchTask: true
    }
  },
  uncss: {
    css: dest + '/css/',
    cssDest: dest + '/',
    html: [url],
    ignore: []
  },
  sass: {
    src: src + "/scss/**/*.scss",
    dest: dest + "/css",
    settings: {
      style: 'compressed'
      //indentedSyntax: true, // Enable .sass syntax!
      //imagePath: './assets/images' // Used by the image-url helper
      //errLogToConsole: true
    }
  },
  images: {
    src: src + "/images/*.{png,gif,jpg}",
    dest: dest + "/images"
  },
  /*markup: {
    src: src + "/htdocs/**",
    dest: dest
  },*/
  iconFonts: {
    name: 'Gulp Starter Icons',
    src: src + '/icons/*.svg',
    dest: dest + '/iconfonts',
    sassDest: src + '/scss/05_base',
    template: './gulp/tasks/iconFont/template.sass.swig',
    sassOutputName: '_iconfonts.sass',
    fontPath: '../iconfonts',
    className: 'icon',
    options: {
      fontName: 'Post-Creator-Icons',
      appendCodepoints: true,
      normalize: true,
      fontHeight: 1001
    }
  },
  scripts: {
    mainSrc: src + '/js/main/*.js',
    src: src + '/js/**/*.js',
    dest: dest + '/js/'
  },
  production: {
    cssSrc: dest + '/css/*.css',
    cssDest: dest + '/css',
    jsSrc: dest + '/js/*.js',
    jsDest: dest + '/js'
  }
};
