/*
   Main JS
   ========================================================================== */

//= include ../../bower_components/jquery/dist/jquery.js
//= include ../../bower_components/jspdf/dist/jspdf.debug.js
//= include ../../bower_components/shariff/build/shariff.complete.js
//= include ../../bower_components/photoswipe/dist/photoswipe.min.js
//= include ../../bower_components/photoswipe/dist/photoswipe-ui-default.min.js
//= include ../vendors/p5.js
//= include ../getaway/getaway.js
//= include ../vendors/jquery.masonry.min.js
//= include ../gallery/mainGallery.js
//= include ../gallery/photoswipe.js

var dur = 200; // animation duration

var hudClone = $('.hud').clone(true); // fuers zuruecksetzen bei newGame()
var imageSavedOnline = false; // damit man nicht zweimal in galerie speichern kann

$(document).ready(function() {
	bindEvents();
});

function bindEvents() {
	// hover-svg-links
	$('.hvr').hover(function() {
		// rot machen
		$('.hvr0', this).hide();
		$('.hvr1', this).show();
		}, function() {
		// wieder weiss machen
		$('.hvr1', this).hide();
		$('.hvr0', this).show();
	});

	$('a.link').hover(function() {
		$('a.link').removeClass('hovered');
		$(this).addClass('hovered');
	}, function() {
		$(this).removeClass('hovered');
	});

	// new game
	$('.new-game').click(function(evt) {
		//console.log("bleep bloop");
		newGame();
		evt.stopPropagation();
	});

	// go to save menu
	$('.save-graphic').click(function(evt) {
		hudControl.hudMenuSubMenu = 1;
		hudControl.hudMenuPos = 0;

		$('.hud-menu-main, .hud-landscape-social-wrapper').fadeOut(dur, function() {
			$('.hud-menu-save').fadeIn(dur);
		});

		evt.stopPropagation();
	});

	// go back to main menu from save graphic
	$('.cancel-save').click(function(evt) {
		hudControl.hudMenuSubMenu = 0;
		hudControl.hudMenuPos = 1;

		$('.hud-menu-save, .hud-gallery-thumb-wrapper').fadeOut(dur, function() {
			$('.hud-menu-main, .hud-landscape-social-wrapper').fadeIn(dur);
		});

		evt.stopPropagation();
	});

	// pdf download button
	$('.pdfdownload').click(function(evt) {
	  getPdf();
	  evt.stopPropagation();
	});

	// image download button
	$('.imagedownload').click(function(evt) {
		getImage();
	  evt.stopPropagation();
	});

	// save to gallery button
	$('.savetogallery').click(function(evt) {
		if (!imageSavedOnline) saveToGallery();
		evt.stopPropagation();
	});

	// go to how to play menu
	$('.how-to-play').click(function(evt) {
		hudControl.hudMenuSubMenu = 2;
		hudControl.hudMenuPos = 0;

		$('.hud-menu-main, .hud-landscape-social-wrapper').fadeOut(dur, function() {
			$('.hud-menu-howto').fadeIn(dur);
		});

		evt.stopPropagation();
	});

	// go back to main menu from how to play
	$('.cancel-howto').click(function(evt) {
		hudControl.hudMenuSubMenu = 0;
		if (!hasGameEverBeenRunning) hudControl.hudMenuPos = 1;
		else hudControl.hudMenuPos = 2;

		$('.hud-menu-howto').fadeOut(dur, function() {
			$('.hud-menu-main, .hud-landscape-social-wrapper').fadeIn(dur);
		});

		evt.stopPropagation();
	});

	// go to gallery menu
	$('.gallery').click(function(evt) {
		hudControl.hudMenuSubMenu = 3;
		hudControl.hudMenuPos = 0;

		loadGalleryImages();
		$('.hud-landscape').fadeOut(dur);
		$('.hud-menu-main').fadeOut(dur, function() {
			$('.hud-menu-gallery').fadeIn(dur, function() {
				//$('.hud-hide').delay(1000).trigger("click");
				$('.hud-hide').trigger("click");
			});
		});

		evt.stopPropagation();
	});

	// go back to main menu from gallery
	$('.cancel-gallery').click(function(evt) {
		hudControl.hudMenuSubMenu = 0;
		if (!hasGameEverBeenRunning) hudControl.hudMenuPos = 2;
		else hudControl.hudMenuPos = 3;

		$('.hud-menu-gallery, #gallery').fadeOut(dur, function() {
			$('.hud-menu-main, #getaway').fadeIn(dur);
			landScapeWarning(); // evtl. landscape-warnimg wieder einblenden
			unloadGalleryImages();
		});

		evt.stopPropagation();
	});
}

// hide-hud-button - er wird nicht in bindEvents behandelt, da er nicht in newGame() ausgetauscht wird
var hudHidden = false;
$('.hud-hide').click(function(evt) {
	var hudWidth = $('.hud-main').width() - 80;

	if (!hudHidden) {
		$('.hud-wrapper').animate({
			left: "-480px"
		}, dur);

		$('.hud-hide-main').animate({
			left: "-=" + hudWidth
		}, dur);

		$('.hud-hide-bg').animate({
			opacity: "0.9"
		}, dur);

		$('.hud-hide-wrapper').css({
			'-moz-transform':    'rotate(180deg)',
			'-webkit-transform': 'rotate(180deg)',
			'-o-transform':      'rotate(180deg)',
			'-ms-transform':     'rotate(180deg)',
			'transform':         'rotate(180deg)'
		});
	}
	else {
		$('.hud-wrapper').animate({
			left: "0px"
		}, dur);

		$('.hud-hide-main').animate({
			left: "0px"
		}, dur);

		$('.hud-hide-bg').animate({
			opacity: "0"
		}, dur);

		$('.hud-hide-wrapper').css({
			'-moz-transform':    'rotate(0deg)',
			'-webkit-transform': 'rotate(0deg)',
			'-o-transform':      'rotate(0deg)',
			'-ms-transform':     'rotate(0deg)',
			'transform':         'rotate(0deg)'
		});
	}

	hudHidden = !hudHidden;
	evt.stopPropagation();
});


// das ende von window resize abwarten, dann canvas neu malen
var rtime = new Date(1, 1, 2000, 12, 0, 0);
var timeout = false;
var delta = 200;
$(window).resize(function() {
    rtime = new Date();
    if (timeout === false) {
        timeout = true;
        setTimeout(resizeEnd, delta);
    }
});

function resizeEnd() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnd, delta);
    } else {
        timeout = false;
				windowResizeHasEnded();

				if (hudHidden) {
					hudWidth = $('.hud-main').width() - 80;

					$('.hud-hide-main').animate({
						left: "-" + hudWidth
					}, dur);
				}
    }
}

// fuers extrahieren von parametern aus urls
function gup( name, url ) {
  if (!url) url = location.href
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( url );
  return results == null ? null : results[1];
}