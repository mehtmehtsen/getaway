var itemsTemp = [];
var items;
var $pswp;

var lightBox;

function initPhotoswipe() {
  $('.grid-item').each( function() {
    var $pic = $(this);

    var getItems = function() {
      $pic.find('a.imglink').each(function() {
        var $href = $(this).attr('href'),
          $id     = $(this).attr('id'),
          $size   = $(this).data('size').split('x'),
          $width  = $size[0],
          $height = $size[1];

        //console.log($width);
        var item = {
          src : $href,
          w   : $width,
          h   : $height,
          pid : $id
        }

        itemsTemp.push(item);

        $(this).addClass('addedToPswp');
      });
      return itemsTemp;
    }

    items = getItems();
  });

  $pswp = $('.pswp')[0];

  bindPhotoswipeEvents();
}

function addToPhotoswipe() {
  itemsTemp = [];

  $('.grid-item').each( function() {
    var $pic = $(this);

    var getItems = function() {
      $pic.find('a.imglink').not('.addedToPswp').each(function() {
        var $href = $(this).attr('href'),
          $id     = $(this).attr('id'),
          $size   = $(this).data('size').split('x'),
          $width  = $size[0],
          $height = $size[1];

        //console.log($width);
        var item = {
          src : $href,
          w   : $width,
          h   : $height,
          pid : $id
        }

        itemsTemp.push(item);

        $(this).addClass('addedToPswp');
      });
      return itemsTemp;
    }

    newItems = getItems();
  });
  //console.log(lightBox);

  for (var i = 0; i < newItems.length; i++) items.push(newItems[i]);

  bindPhotoswipeEvents();
}

function bindPhotoswipeEvents() {
  $('.imglink').click(function(event) {
    event.preventDefault();

    var $index = $(this).parent().index();

    // imageUrl generieren fuer shareButtons. evtl "#" wegmachen
    var imageUrl = window.location.href;// + $(this).attr("href");
    var hashLoc = imageUrl.indexOf("#");
    if (hashLoc > 0) imageUrl = imageUrl.substr(0, hashLoc);
    imageUrl += $(this).attr("href");
    //console.log("HREF: " + imageUrl);

    var options = {
        index: $index,
        bgOpacity: 0.8,
        showHideOpacity: true,
        shareButtons: [
          {id:'facebook', label: '&nbsp;', url:'https://www.facebook.com/sharer/sharer.php?u={{url}}'},
          {id:'twitter', label: '&nbsp;', url:'https://twitter.com/intent/tweet?text=I%20just%20made%20this%20picture%20with%20getAway%20by%20Komeht%20&url={{url}}'},
          {id:'pinterest', label: '&nbsp;', url:'http://pinterest.com/pin/create/button/?url={{url}}&media={' + imageUrl + '}&description={I%20just%20made%20this%20picture%20with%20getAway%20by%20Komeht%20}'},
        ]
        //history: false
    }

    // Initialize PhotoSwipe
    lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
    lightBox.init();

    lightBox.invalidateCurrItems();
    lightBox.updateSize(true);

    lightBox.listen('destroy', function() {

    });
  });
}

//"http://pinterest.com/pin/create/button/?url={{url}}&media={URI-encoded URL of the image to pin}&description={optional URI-encoded description}"