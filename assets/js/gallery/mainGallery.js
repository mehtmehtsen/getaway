var blinkInterval = null;
var blinkIntervalLoadGalleryImages = null;

function saveToGallery() {
	var getawayCanvas = $('div#getaway > canvas#defaultCanvas');
	var canvasData = getawayCanvas[0].toDataURL("image/png");

	var obj = $('.saving');

	$.ajax({
	  type: "POST",
	  url: "./gallery/saveimage.php",
	  data: canvasData,
	  cache: true,
		datatype: 'json',
		beforeSend: function(xhr){
			xhr.setRequestHeader('Content-Type', 'application/upload');

			$('.savetogallery').hide();
			$(obj).show();
			//$(obj).find('.hud-menu-pt-area').text('saving...');
			//$(obj).addClass('blinking-grey');

			blinkInterval = setInterval(function() {
				if ($(obj).hasClass('blinking-red')) $(obj).removeClass('blinking-red');
				else $(obj).addClass('blinking-red');
			}, dur);
		},
	  success: function(returnedData) {
	  	//console.log(returnedData);
			var arr = $.parseJSON(returnedData);

	    $(obj).removeClass('blinking-red');
			clearInterval(blinkInterval);
			$(obj).hide();
			$('.saved').show();
			$('.hud-gallery-thumb-wrapper').show();

			imageSavedOnline = true;

			loadSavedImage(arr);
	  },
		error: function() {
			//console.log("kapott");

			$(obj).removeClass('blinking-red');
			clearInterval(blinkInterval);
			$(obj).hide();
			$('.error').show();
		}
	});
}

function loadSavedImage(arr) {
	var maxWidth = 180;
	var maxHeight = 180;
	var imgWidth = arr[2];
	var imgHeight = arr[3];

	if (imgWidth > imgHeight) {
		imgHeight *= maxWidth / imgWidth;
		imgWidth = maxWidth;
	}
	else {
		imgWidth *= maxHeight / imgHeight;
		imgHeight = maxHeight;
	}

	//$('.gallery-thumb').html("<a href='" + arr[1] + "'><img src='" + arr[0] + "' width='" + imgWidth + "' height='" + imgHeight + "'></a>");
	//$('.gallery-thumb').html("<a href='#&gid=1&pid=" + arr[5] + "'><img src='" + arr[0] + "' width='" + imgWidth + "' height='" + imgHeight + "'></a>"); // adds hash, doesn't reload
	$('.gallery-thumb').html("<a href='javascript:;'><img src='" + arr[0] + "' width='" + imgWidth + "' height='" + imgHeight + "'></a>");

	$('.gallery-thumb').click(function() {
		window.location.hash = "#&gid=1&pid=" + arr[5];
		location.reload();
	});
}

//var galleryLoaded = false;
var galleryPage = 0;

function loadGalleryImages() {
	var dataString = "page=" + galleryPage;
	var obj = $('.loading');

	$.ajax({
		type: "GET",
		url: "./gallery/gallery_get.php",
		data: dataString,
		cache: false,

		beforeSend: function(xhr){
			//console.log("beforeSend");
			$('.load-more').hide();
			$(obj).show();

			blinkIntervalLoadGalleryImages = setInterval(function() {
				if ($(obj).hasClass('blinking-red')) $(obj).removeClass('blinking-red');
				else $(obj).addClass('blinking-red');
				//console.log("blink blunk");
			}, dur);
		},
		success: function(returnedData) {
			//console.log("gallery-page " + galleryPage + " loaded");

	    $(obj).removeClass('blinking-red');
			clearInterval(blinkIntervalLoadGalleryImages);
			$(obj).hide();
			$('.load-more').show();

			if (galleryPage === 0) { // getaway aus- und galerie einblenden, masonry initialisieren und loadMoreButton binden
				$("#getaway").fadeOut(dur, function() {
					$(".gallery-grid").html(returnedData);
					$("#gallery").fadeIn(dur);
					masonryzifyGrid();
					bindLoadMore();
					bindLikeEvents();
				});
			}
			else { // neue bilder masonry hinzufuegen
				$jQueryReturnedData = $(returnedData);
				$grid.append($jQueryReturnedData).masonry('appended', $jQueryReturnedData);

				// neue bilder photoswipe hinzufuegen
				addToPhotoswipe();

				bindLikeEvents();
			}

			galleryPage++;
		},
		error: function() {
			//console.log("errorrrrr");

			$(obj).removeClass('blinking-red');
			clearInterval(blinkIntervalLoadGalleryImages);
			$(obj).hide();
			$('.loading-error').show();
		}

	});

	$("body").css("overflow", "scroll");
}

// go to gallery menu
function loadGalleryForIndividualImage(imgId) {
	hudControl.hudMenuSubMenu = 3;
	hudControl.hudMenuPos = 0;

	loadGalleryImagesIndividual(imgId);
	$('.hud-landscape').fadeOut(dur);
	$('.hud-menu-main').fadeOut(dur, function() {
		$('.hud-menu-gallery').fadeIn(dur, function() {
			//$('.hud-hide').delay(1000).trigger("click");
			$('.hud-hide').trigger("click");
		});
	});
}

function loadGalleryImagesIndividual(imgId) {
	var dataString = "id=" + imgId;
	var obj = $('.loading');

	$.ajax({
		type: "GET",
		url: "./gallery/gallery_get_individual.php",
		data: dataString,
		cache: false,

		beforeSend: function(xhr){
			//console.log("beforeSend");
			$('.load-more').hide();
			$(obj).show();

			blinkIntervalLoadGalleryImages = setInterval(function() {
				if ($(obj).hasClass('blinking-red')) $(obj).removeClass('blinking-red');
				else $(obj).addClass('blinking-red');
				console.log("blink blunk");
			}, dur);
		},
		success: function(returnedData) {
			//console.log("gallery-page " + galleryPage + " loaded");

	    $(obj).removeClass('blinking-red');
			clearInterval(blinkIntervalLoadGalleryImages);
			$(obj).hide();
			$('.load-more').show();

			if (galleryPage === 0) { // getaway aus- und galerie einblenden, masonry initialisieren und loadMoreButton binden
				$("#getaway").fadeOut(dur, function() {
					$(".gallery-grid").html(returnedData);
					$("#gallery").fadeIn(dur);
					masonryzifyGrid();
					bindLoadMore();
					bindLikeEvents();

					$(".imglink[data-imgid='" + imgId + "']").trigger("click"); // it works! it fucking works!1
				});
			}
			else { // neue bilder masonry hinzufuegen
				$jQueryReturnedData = $(returnedData);
				$grid.append($jQueryReturnedData).masonry('appended', $jQueryReturnedData);

				// neue bilder photoswipe hinzufuegen
				addToPhotoswipe();

				bindLikeEvents();

			}

			galleryPage++;
		},
		error: function() {
			//console.log("errorrrrr");

			$(obj).removeClass('blinking-red');
			clearInterval(blinkIntervalLoadGalleryImages);
			$(obj).hide();
			$('.loading-error').show();
		}

	});

	$("body").css("overflow", "scroll");
}

function unloadGalleryImages() {
  $grid.masonry().masonry("destroy");
	$("body").scrollTop(0);
	$("body").scrollLeft(0);
	$("body").css("overflow", "hidden");

	$(".gallery-grid").html("");

	galleryPage = 0;
}

var masonryOptions = {
  itemSelector : '.grid-item',
  columnWidth : 240,
  isAnimated: false
};

var $grid;

function masonryzifyGrid() {
  //console.log("krrh");
  $grid = $('.gallery-grid').masonry(masonryOptions);
  initPhotoswipe();
}

function bindLikeEvents() {
	$(".like").unbind();
  $(".like").click(function() {
  	console.log("clicked");
    var dies = $(this);
    var id = $(this).attr("id");
    var dataString = 'id='+ id ;
    //var parent = $(this);

    //$(this).fadeIn(200).html('<img src="dot.gif" />');
    $.ajax({
      type: "POST",
      url: "./gallery/gallery_like.php",
      data: dataString,
      cache: false,
      success: function(html) {
        var l = parseInt($(dies).find('#likes').text());
        if (html == '0') { // schon gevotet
          var nope = $(dies).find('#nope');
          $(nope).show().delay(1000).fadeOut(2000);
        }
        else { // vote abgegeben
          $(dies).find('.heart').hide();
          $(dies).find('.heart-voted').show();
          l++;
        }
        $(dies).find('#likes').text(l + "");
      }
    });

    return false;
  });
}

function bindLoadMore() {
  $('.load-more-btn').click(function() {
  	//console.log("hallohier");
		loadGalleryImages();
	});
}
