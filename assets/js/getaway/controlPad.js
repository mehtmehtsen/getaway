function ControlPad() {
  var mitte;
  var prevMitte; // zum uebermalen
  var pressed;

  var grenzen;
  var pad;
  var dir;
}

ControlPad.prototype.setup = function() {
  this.mitte = createVector(0, 0);
  this.prevMitte = createVector(this.mitte.x, this.mitte.y);
  this.pressed = false;

  this.grenzen = [];
  this.pad = [];
  this.dir = 0;

  for (var i = 0; i < 8; i++) {
    this.grenzen = append(this.grenzen, (TWO_PI / 16) * ((i * 2) + 1));

    var padPt = createVector(0, 1);
    padPt.rotate(this.grenzen[i]);
    padPt.mult(50);
    this.pad = append(this.pad, padPt);
  }
  //print(this.grenzen[4] + ", " + this.grenzen[4]);

}

ControlPad.prototype.deDraw = function() {
  if (this.pressed) { // uebermalen
    push();
    fill(255);
    stroke(255);
    //strokeWeight(2);

    translate(this.mitte.x, this.mitte.y);

    // achteck
    beginShape();
    for (var i = 0; i < this.pad.length; i++) {
      vertex(this.pad[i].x, this.pad[i].y);
    }
    endShape();

    pop();
  }
}

ControlPad.prototype.update = function() {
  if (touchDown) {
    if (!this.pressed) {
      this.pressed = true;
      this.mitte.set(tX, tY);
    }

    var mouse = createVector(tX, tY);
    var diff = createVector(this.mitte.x, this.mitte.y);
    diff.sub(mouse);

    var direction = angle(diff.x, diff.y);

    touch_left = false;
    touch_right = false;
    touch_up = false;
    touch_down = false;

    if (direction < this.grenzen[0]) {
      touch_left = true;
      dir = 1;
    }
    if (direction > this.grenzen[0] && direction < this.grenzen[1]) {
      touch_left = true;
      touch_up = true;
      dir = 2;
    }
    if (direction > this.grenzen[1] && direction < this.grenzen[2]) {
      touch_up = true;
      dir = 3;
    }
    if (direction > this.grenzen[2] && direction < this.grenzen[3]) {
      touch_right = true;
      touch_up = true;
      dir = 4;
    }
    if (direction > this.grenzen[3] && direction < this.grenzen[4]) {
      touch_right = true;
      dir = 5;
    }
    if (direction > this.grenzen[4] && direction < this.grenzen[5]) {
      touch_right = true;
      touch_down = true;
      dir = 6;
    }
    if (direction > this.grenzen[5] && direction < this.grenzen[6]) {
      touch_down = true;
      dir = 7;
    }
    if (direction > this.grenzen[6] && direction < this.grenzen[7]) {
      touch_left = true;
      touch_down = true;
      dir = 0;
    }
    if (direction > this.grenzen[7]) {
      touch_left = true;
      dir = 1;
    }

    push();
    fill(0, 10);
    noStroke();

    translate(this.mitte.x, this.mitte.y);

    // achteck
    beginShape();
    for (var i = 0; i < this.pad.length; i++) {
      vertex(this.pad[i].x, this.pad[i].y);
    }
    endShape();

    // richtungs-dreieck
    dirPt0 = dir;
    dirPt1 = dir + 1;
    if (dirPt1 > 7) dirPt1 = 0;

    beginShape();
    vertex(this.pad[dirPt0].x, this.pad[dirPt0].y);
    vertex(this.pad[dirPt1].x, this.pad[dirPt1].y);
    vertex(0, 0);
    endShape();

    fill(0);
    pop();
  }
  else {
    this.pressed = false;
    touch_left = false;
    touch_right = false;
    touch_up = false;
    touch_down = false;
  }
}

function touchStarted() {
  tX = touchX;
  tY = touchY;
  touchDown = true;
  if (gameRunning) return false;
}

function touchMoved() {
  tX = touchX;
  tY = touchY;
  if (gameRunning) return false;
}

function touchEnded() {
  touchDown = false;
  if (gameRunning) return false;
}
