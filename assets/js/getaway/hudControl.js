function HudControl() {
  this.keyboardUser = false;

  this.hudMenuSubMenu = 0;
  this.hudMenuPos = 0;
}

HudControl.prototype.reset = function() {
  this.hudMenuMain = $('.hud-menu-main').find('.hud-menu-pt').has('a:not(.invisible)');
  this.hudMenuSave = $('.hud-menu-save').find('.hud-menu-pt').has('a');
  this.hudMenuHowto = $('.hud-menu-howto').find('.hud-menu-pt').has('a');
  this.hudMenuGallery = $('.hud-menu-gallery').find('.hud-menu-pt').has('a');
  this.hudMenu = [this.hudMenuMain, this.hudMenuSave, this.hudMenuHowto, this.hudMenuGallery];
  //console.log("laenge des ersten menus: " + this.hudMenu[0].length);

  this.hudMenuSubMenu = 0;
  this.hudMenuPos = 0;
}

HudControl.prototype.update = function() {
}

HudControl.prototype.handleKeyPress = function(keyCode) {
  if (this.keyboardUser) {
    // up/down handeln
    if (keyCode === UP_ARROW || keyCode == 87) this.hudMenuPos--;
    if (keyCode === DOWN_ARROW || keyCode == 83) this.hudMenuPos++;
    if (this.hudMenuPos < 0) this.hudMenuPos = this.hudMenu[this.hudMenuSubMenu].length - 1;
    if (this.hudMenuPos > this.hudMenu[this.hudMenuSubMenu].length - 1) this.hudMenuPos = 0;

    // enter handeln
    if (keyCode == 32 || keyCode === ENTER || keyCode === RETURN) {
      $(this.hudMenu[this.hudMenuSubMenu][this.hudMenuPos]).find('a')[0].click();
    }

    // menu-punkte fake-hovern
    $(this.hudMenu[this.hudMenuSubMenu]).find('a').removeClass('hovered');
    $(this.hudMenu[this.hudMenuSubMenu][this.hudMenuPos]).find('a').addClass('hovered');
  }
  else {
    // ersten keypress aller zeiten handeln
    this.hudMenuPos = 0;

    $(this.hudMenu[this.hudMenuSubMenu]).find('a').removeClass('hovered');
    $(this.hudMenu[this.hudMenuSubMenu][this.hudMenuPos]).find('a').addClass('hovered');

    this.keyboardUser = true;
  }
}
