function SpawnAnimation() {
  var pos;
  var prevPos;
  var front;
  var left;
  var right;
  var dur = 15;
  var age = 15;
  var col;
}

SpawnAnimation.prototype.setup = function(pos, front, left, right, col) {
  this.pos = pos;
  this.prevPos = createVector(this.pos.x, this.pos.y);
  this.front = front;
  this.left = left;
  this.right = right;
  this.col = col;

  this.dur = 15;
  this.age = 0;
}

SpawnAnimation.prototype.deDraw = function() {
  strokeWeight(map(this.age, 0, this.dur, 1, 12));
  stroke(255);
  triangle(this.front.x, this.front.y, this.right.x, this.right.y, this.left.x, this.left.y);
}

SpawnAnimation.prototype.update = function() {
  if (this.age < this.dur) {
    noFill();
    strokeWeight(map(this.age, 0, this.dur, 1, 12));
    stroke(red(this.col), green(this.col), blue(this.col), map(this.age, 0, this.dur, 100, 0));
    triangle(this.front.x, this.front.y, this.right.x, this.right.y, this.left.x, this.left.y);
    this.age++;
  }
}

