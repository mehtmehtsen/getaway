function TimeWarpDraw() {
  //this.t;
  //this.started;
  //this.done;
  this.playerStrokeWeight = 1.2;
  this.enemyStrokeWeight = 0.5;
  this.circleOfDeathStroke = 1;
  this.circleCounter = 0;
}

TimeWarpDraw.prototype.setup = function() {
  this.t = 0; // zaehlt den fortschritt
  this.started = false;
  this.done = false;
}

TimeWarpDraw.prototype.update = function() {
  if (!this.done) {
    if (this.t === 0) {
      background(bgColor);

      // spielertod zeichnen (das gleiche unten nochmal, wegen ueberdeckenden gegnerlinien
      strokeCap(ROUND);
      stroke(playerColor);
      strokeWeight(this.circleOfDeathStroke);
      noFill();
      var killPos = createVector(parseInt(player.positions[player.positions.length - 1].x), parseInt(player.positions[player.positions.length - 1].y));
      ellipse(killPos.x + .5, killPos.y + .5, 10.5, 10.5);
      line(killPos.x - 3.5, killPos.y - 3.5, killPos.x + 3.5, killPos.y + 3.5);
      line(killPos.x + 3.5, killPos.y - 3.5, killPos.x - 3.5, killPos.y + 3.5);
    }
    if (this.t <= player.alive) {
      // gegner zeichnen
      strokeWeight(this.enemyStrokeWeight);//1);
      stroke(enemyColor);

      strokeCap(ROUND);
      noFill();

      for (var i = 0; i < enemies.length; i++) { // gegner nacheinander durchgehen
        var j = enemies[i].positions.length - this.t - 1; // beim juengsten punkt anfangen
        if (j > 1) line(enemies[i].positions[j - 1].x, enemies[i].positions[j - 1].y, enemies[i].positions[j].x, enemies[i].positions[j].y);
      }

      // spieler zeichnen
      var j = player.alive - this.t - 1;

      // circles berechnen
      if (j > 0 && player.positions[j - 1].x == player.positions[j].x && player.positions[j - 1].y == player.positions[j].y) {
        this.circleCounter++;

        // am anfang eines spiels keinen circle zeichnen
        if (player.positions[j].x == player.startPos.x && player.positions[j].y == player.startPos.y) this.circleCounter = 0;
        if (this.circleCounter > 45) this.circleCounter = 45;
        fill(0);
        noStroke();
        ellipse(player.positions[j].x, player.positions[j].y, this.circleCounter / 3, this.circleCounter / 3);
      }
      else this.circleCounter = 0;

      j = constrain(j, 2, player.alive);
      strokeCap(ROUND);
      strokeWeight(this.playerStrokeWeight);
      stroke(0);
      line(player.positions[j].x, player.positions[j].y, player.positions[j - 1].x, player.positions[j - 1].y);

      this.t++;
    }
    else {
      // spielertod zeichnen
      strokeCap(ROUND);
      stroke(playerColor);
      strokeWeight(this.circleOfDeathStroke);
      noFill();
      var killPos = createVector(parseInt(player.positions[player.positions.length - 1].x), parseInt(player.positions[player.positions.length - 1].y));
      ellipse(killPos.x + .5, killPos.y + .5, 10.5, 10.5);
      line(killPos.x - 3.5, killPos.y - 3.5, killPos.x + 3.5, killPos.y + 3.5);
      line(killPos.x + 3.5, killPos.y - 3.5, killPos.x - 3.5, killPos.y + 3.5);

      // spielergeburt zeichnen
      fill(200, 0, 40);
      stroke(200, 0, 40);
      var middle = createVector(player.startPos.x, player.startPos.y);

      var frontNow = createVector(0, 5);
      frontNow.rotate(player.firstAngle - HALF_PI);
      frontNow.add(middle);
      var leftNow = createVector(-4, -5);
      leftNow.rotate(player.firstAngle - HALF_PI);
      leftNow.add(middle);
      var rightNow = createVector(4, -5);
      rightNow.rotate(player.firstAngle - HALF_PI);
      rightNow.add(middle);

      triangle(frontNow.x, frontNow.y, rightNow.x, rightNow.y, leftNow.x, leftNow.y);

      this.done = true;
    }
  }
}

TimeWarpDraw.prototype.drawPdf = function() {
  var pdfT = 0; // zaehlt den fortschritt
  var pdfDone = false;
  var pdfCircleCounter = 0;

  while (!pdfDone) {
    if (pdfT <= player.alive) {
      // gegner zeichnen
      mehtsPdf.setLineWidth(this.enemyStrokeWeight);
      mehtsPdf.setDrawColor(red(enemyColor), green(enemyColor), blue(enemyColor));

      mehtsPdf.setLineCap('round');

      for (var i = 0; i < enemies.length; i++) { // gegner nacheinander durchgehen
        var j = enemies[i].positions.length - pdfT - 1; // beim juengsten punkt anfangen
        if (j > 1) mehtsPdf.line(enemies[i].positions[j - 1].x, enemies[i].positions[j - 1].y, enemies[i].positions[j].x, enemies[i].positions[j].y);
      }

      // spieler zeichnen
      var j = player.alive - pdfT - 1;

      // circles berechnen
      if (j > 0 && player.positions[j - 1].x == player.positions[j].x && player.positions[j - 1].y == player.positions[j].y) {
        pdfCircleCounter++;

        // am anfang eines spiels keinen circle zeichnen
        if (player.positions[j].x == player.startPos.x && player.positions[j].y == player.startPos.y) pdfCircleCounter = 0;
        if (pdfCircleCounter > 45) pdfCircleCounter = 45;
        //fill(0);
        //noStroke();
        var cc = parseInt(pdfCircleCounter / 6);
        //console.log(pdfCircleCounter + " -> " + cc);
        mehtsPdf.circle(player.positions[j].x, player.positions[j].y, cc, 'F');
      }
      else pdfCircleCounter = 0;

      j = constrain(j, 2, player.alive);
      mehtsPdf.setLineWidth(this.playerStrokeWeight);
      mehtsPdf.line(player.positions[j].x, player.positions[j].y, player.positions[j - 1].x, player.positions[j - 1].y);

      pdfT++;
    }
    else {
      // spielertod zeichnen
      mehtsPdf.setDrawColor(red(playerColor), green(playerColor), blue(playerColor));

      mehtsPdf.setLineWidth(this.circleOfDeathStroke);
      var killPos = createVector(parseInt(player.positions[player.positions.length - 1].x), parseInt(player.positions[player.positions.length - 1].y));
      mehtsPdf.circle(killPos.x + .5, killPos.y + .5, 5.25, 'D');
      mehtsPdf.line(killPos.x - 3, killPos.y - 3, killPos.x + 4, killPos.y + 4);
      mehtsPdf.line(killPos.x + 4, killPos.y - 3, killPos.x - 3, killPos.y + 4);

      // spielergeburt zeichnen
      mehtsPdf.setFillColor(red(playerColor), green(playerColor), blue(playerColor));
      var middle = createVector(player.startPos.x, player.startPos.y);

      var frontNow = createVector(0, 5);
      frontNow.rotate(player.firstAngle - HALF_PI);
      frontNow.add(middle);
      var leftNow = createVector(-4, -5);
      leftNow.rotate(player.firstAngle - HALF_PI);
      leftNow.add(middle);
      var rightNow = createVector(4, -5);
      rightNow.rotate(player.firstAngle - HALF_PI);
      rightNow.add(middle);

      mehtsPdf.triangle(frontNow.x, frontNow.y, rightNow.x, rightNow.y, leftNow.x, leftNow.y, 'FD');

      pdfDone = true;
    }
  }
}
