//= include ./player.js
//= include ./enemy.js
//= include ./spawnAnimation.js
//= include ./timeWarp.js
//= include ./keys.js
//= include ./controlPad.js
//= include ./hudControl.js

var player;
var enemies;

var playerColor;
var enemyColor;
var bgColor;

var lastLoop = new Date;
var count = 0;
var q = 0;

var killer = -1;

var hasGameEverBeenRunning = false;
var gameRunning = false;
var moveTimer = 120;
var moveOn = false;

var godmode = false;

var spawnAnimation;

var timeWarpDraw;
var timeWarpTimer = 120; // zwei sekunden (120 frames) nach timeWarpDraw.done wird hud wieder angezeigt

var controlPad;

var key_left = false;
var key_right = false;
var key_up = false;
var key_down = false;
var touch_left = false;
var touch_right = false;
var touch_up = false;
var touch_down = false;

var key_space = false;

var isLandscape;
var isTouch;
var touchDown = false;
var tX = 0;
var tY = 0;

var hudActive = true; // fuer hud-keyboard-controls
var hudControl;

function setup() {
  var mehtsCanvas = createCanvas(windowWidth, windowHeight);

  mehtsCanvas.parent('getaway');

  frameRate(120);
  colorMode(RGB, 255, 255, 255, 100);

  player = new Player();
  player.dead = true;

  hudControl = new HudControl();
  hudControl.reset();

  isLandscape = isLandscapeMode();
  isTouch = isTouchDevice();

  landScapeWarning();

  rectMode(CENTER);

  //console.log("gesetarped");

  // galerie oeffnen, wenn in url photoswipe-zeuchs drinsteckt
  // dies geschieht hier, weil hudControl vorher nicht existiert
  var url = window.location.href;
  if (url.indexOf("#&") >= 0) {
    window.location.hash = '#'; // url saeubern, sonst verhaelt sich photoswipe komisch
    var imgId = gup('pid', url);
    loadGalleryForIndividualImage(imgId);
  }
}

function newGame() {
  hudActive = false;

  $('.getaway-fade-out').fadeIn(300, function() {
    background(255);
    $('.getaway-fade-out').hide();

    $('.hud-wrapper').fadeOut(300, function() { // erst hud wegfaden
      player = new Player();
      player.setup();

      enemies = [];

      playerColor = color(200, 0, 40);
      enemyColor = color(0);
      bgColor = color(255);

      spawnAnimation = new SpawnAnimation();
      spawnAnimation.setup(createVector(-50, 0), createVector(0, 0), createVector(0, 0), createVector(0, 0), color(0));

      timeWarpDraw = new TimeWarpDraw();
      timeWarpTimer = 0;

      controlPad = new ControlPad();
      controlPad.setup();

      hasGameEverBeenRunning = true;
      gameRunning = true;
      moveTimer = 0;
      moveOn = false;

      //console.log("hier geht was");
    });
  });

  // hud zuruecksetzen
  $('.hud').replaceWith(hudClone.clone());
  imageSavedOnline = false;
  bindEvents();
  if (hasGameEverBeenRunning) {
    $('.greyed-out').hide();
    $('.not-greyed-out').show();
    $('a.link').removeClass('invisible');
  }
}

function draw() {
  //if (frameCount % 60 === 0) print(frameRate() + " fps");

  //print(hudControl.hudMenuSubMenu + " | " + hudControl.hudMenuPos);

  //if (hudActive) hudControl.update();

  if (hasGameEverBeenRunning) {
    if (gameRunning) { // spiel laeuft
      //background(bgColor);

      // roter Rand for teh Auschecking
      /*stroke(255, 0, 0);
      strokeWeight(3);
      noFill();
      rect(0, 0, width, height);
      strokeWeight(1);*/

      if (moveTimer < 120) {
        var moveOnCheck = false;
        if (moveTimer % 10 === 0) {
          moveOn = !moveOn;
          moveOnCheck = true;
        }
        if (moveOnCheck) {
          if (moveOn) $('.getaway-move').show();
          else $('.getaway-move').hide();
        }
        moveTimer++;
      }

      // alle 40 Frames neuen Gegner erstellen
      if (player.alive % 40 === 0 && player.alive > 80) {
        var enemyPos = createVector(random(width / 20, width - (width / 20)), random(height / 20, height - (height / 20)));
        var playerDist = createVector(enemyPos.x, enemyPos.y);
        playerDist.sub(player.pos);

        if (playerDist.mag() < 100) { // wenn zu nahe am spieler
          playerDist.normalize();
          playerDist.mult(100);
          enemyPos.set(player.pos.x, player.pos.y, 0);
          enemyPos.add(playerDist);
          if (enemyPos.x > width - (width / 20)) enemyPos.x = width - (width / 20);
          if (enemyPos.x < width / 20) enemyPos.x = width / 20;
          if (enemyPos.y > height - (height / 20)) enemyPos.y = height - (height / 20);
          if (enemyPos.y < height / 20) enemyPos.y = height / 20;
        }
        var maxSpeed = map(enemies.length, 0, 60, player.maxSpeed - 1.3, player.maxSpeed + 1.3); // maxSpeed wird immer hoeher
        enemies = append(enemies, new Enemy());
        enemies[enemies.length - 1].setup(enemies.length, enemyPos, maxSpeed);
      }

      // if (godmode && key_space && player.alive > 100) {
      //   if (enemies.length > 40) killer = enemies.length - 40;
      //   player.die();
      // }

      // ALTE SPRITES MIT WEISS UEBERMALEN (NO BACKGROUND(255) ANY MORE)
      spawnAnimation.deDraw();
      player.deDraw();

      fill(bgColor);
      noStroke();
      for (var i = 0; i < enemies.length; i++) enemies[i].deDraw();

      controlPad.deDraw();
      if (isTouch) controlPad.update();

      // NEUE SPRITES MALEN
      noFill();
      stroke(enemyColor);
      strokeWeight(1);
      for (var i = 0; i < enemies.length; i++) enemies[i].update();

      player.update();

      spawnAnimation.update();
      //point(100, 100);
    }
    else { // dh. if (!gameRunning)
      if (!timeWarpDraw.started) {
        timeWarpDraw.setup();
        timeWarpDraw.started = true;

        background(bgColor);
      }
      else { // dh. if (timeWarpDraw.started)
        if (!timeWarpDraw.done) timeWarpDraw.update();
        if (!timeWarpDraw.done) timeWarpDraw.update();
        if (!timeWarpDraw.done) timeWarpDraw.update();

        if (!timeWarpDraw.done && key_space) { // sofort zu ende zeichnen
          while (!timeWarpDraw.done) timeWarpDraw.update();
        }

        if (timeWarpDraw.done) { // zu ende gezeichnet
          if (timeWarpTimer <= 120) {
            timeWarpTimer++; // geht bis 121, dann wirds hud nur einmal eingefadet
          }
          if (timeWarpTimer == 120) {
            hudActive = true;

            $('.save-graphic-grey').find('.greyed-out').hide();
            $('.save-graphic-grey').find('.not-greyed-out').show();
            $('.save-graphic-grey').find('a.link').removeClass('invisible');

            hudControl.reset();

            $('.hud-wrapper').fadeIn(300);
          }
        }
      }
    }
  }

  // FPS - when this is not visible on canvas, previous code is broken
  /*var thisLoop = new Date;
  var fps = 1000 / (thisLoop - lastLoop);
  lastLoop = thisLoop;
  ++q;
  count += fps;
  noStroke();
  fill(0);
  text('FPS mean ' + ((count / q).toFixed(1)), 5, 15);*/
}

function angle(x, y) { // pvector to angle, aus point2line
  var a = atan2(y, x);
  if (a < 0) {
    a += TWO_PI;
    if (a == TWO_PI) a = 0;
    return a;
  }
  else return a;
}

function windowResized() {
  isLandscape = isLandscapeMode();
  landScapeWarning();
}

function windowResizeHasEnded() {
  resizeCanvas(windowWidth, windowHeight);
  if (hasGameEverBeenRunning && !gameRunning) {
    timeWarpDraw.setup();
    timeWarpDraw.started = true;
    while (!timeWarpDraw.done) timeWarpDraw.update();
  }
}

function isTouchDevice() {
 return (('ontouchstart' in window) || (navigator.MaxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0));
}

function isLandscapeMode() {
  if (windowHeight > windowWidth) return false;
  else return true;
}

function landScapeWarning() {
  //alert("checking landscape mode");
  if (windowWidth <= 479 || windowHeight <= 479) {
    if (!isLandscape) $('.hud-landscape').show();
    else $('.hud-landscape').hide();
  }
  else $('.hud-landscape').hide();
}

function getPdf() {
  var orientation = 'p';
  if (isLandscape) orientation = 'l';
  mehtsPdf = new jsPDF(orientation, 'px', [windowWidth, windowHeight]);

  timeWarpDraw.drawPdf();

  //setTimeout(function() {
  mehtsPdf.save(getFilename() + '.pdf');
  //}, 1000);
}

function getImage() {
  save(getFilename() + '.png');
}

function getFilename() {
  var d = new Date();
  var str = "getaway.komeht.de-" + d.getFullYear() + "" + d.getMonth() + "" + d.getDate() + "" + d.getHours() + "" + d.getMinutes() + "" + d.getSeconds();
  return str;
}