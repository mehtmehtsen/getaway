function Player() {
  this.maxSpeed = 3;
  this.positions = [];
  this.dead = false;
  this.alive = 0;
  this.moved = false;
  this.angle = PI;
  this.firstAngle = -HALF_PI;

  //this.pos;
  //this.startPos;
  //this.speed;
  //this.alive;
  //this.front;
  //this.left;
  //this.right;
  //this.frontNow;
  //this.leftNow;
  //this.rightNow;
  //this.diag;
}

Player.prototype.setup = function() {
  this.pos = createVector(windowWidth / 2, (windowHeight / 2) - 25);
  this.startPos = createVector(this.pos.x, this.pos.y);
  this.speed = createVector(0, -0.1);
  this.alive = 0;
  this.front = createVector(0, 5);
  this.left = createVector(-4, -5);
  this.right = createVector(4, -5);
  this.frontNow = createVector(this.front.x, this.front.y);
  this.leftNow = createVector(this.left.x, this.left.y);
  this.rightNow = createVector(this.right.x, this.right.y);
  this.diag = sin(HALF_PI / 2) * this.maxSpeed;
}

Player.prototype.deDraw = function() {
  // vorherige Position mit weiß uebermalen
  fill(255);
  noStroke();
  if (this.positions.length > 0) rect(this.positions[this.positions.length - 1].x, this.positions[this.positions.length - 1].y, 18, 18);
  fill(playerColor);
}

Player.prototype.update = function() {
  if (!this.dead) {
    this.speed.set(0, 0);

    // spawnAnimation starten
    if (this.positions.length === 0) spawnAnimation.setup(this.pos, this.frontNow, this.leftNow, this.rightNow, playerColor);

    // keys
    if (key_up && !key_left && !key_right) this.speed.add(0, -this.maxSpeed); // oben
    if (key_up && key_right) this.speed.add(this.diag, -this.diag); // rechts oben
    if (key_right && !key_up && !key_down) this.speed.add(this.maxSpeed, 0); // rechts
    if (key_right && key_down) this.speed.add(this.diag, this.diag); // rechts unten
    if (key_down && !key_left && !key_right) this.speed.add(0, this.maxSpeed); // unten
    if (key_down && key_left) this.speed.add(-this.diag, this.diag); // links unten
    if (key_left && !key_up && !key_down) this.speed.add(-this.maxSpeed, 0); // links
    if (key_left && key_up) this.speed.add(-this.diag, -this.diag); // links oben

    // touch
    if (touch_up && !touch_left && !touch_right) this.speed.add(0, -this.maxSpeed); // oben
    if (touch_up && touch_right) this.speed.add(this.diag, -this.diag); // rechts oben
    if (touch_right && !touch_up && !touch_down) this.speed.add(this.maxSpeed, 0); // rechts
    if (touch_right && touch_down) this.speed.add(this.diag, this.diag); // rechts unten
    if (touch_down && !touch_left && !touch_right) this.speed.add(0, this.maxSpeed); // unten
    if (touch_down && touch_left) this.speed.add(-this.diag, this.diag); // links unten
    if (touch_left && !touch_up && !touch_down) this.speed.add(-this.maxSpeed, 0); // links
    if (touch_left && touch_up) this.speed.add(-this.diag, -this.diag); // links oben

    this.pos.add(this.speed);

    // den buehnenrand nicht uebertreten, bitte
    if (this.pos.x > windowWidth) this.pos.x = windowWidth;
    if (this.pos.x < 0) this.pos.x = 0;
    if (this.pos.y > windowHeight) this.pos.y = windowHeight;
    if (this.pos.y < 0) this.pos.y = 0;

    // spieler zeichnen
    if (mag(this.speed.x, this.speed.y) > 0) this.angle = angle(this.speed.x, this.speed.y) - HALF_PI;


    this.frontNow.set(this.front);
    this.frontNow.rotate(this.angle);
    this.frontNow.add(this.pos);

    this.leftNow.set(this.left);
    this.leftNow.rotate(this.angle);
    this.leftNow.add(this.pos);

    this.rightNow.set(this.right);
    this.rightNow.rotate(this.angle);
    this.rightNow.add(this.pos);

    fill(playerColor);
    stroke(playerColor);

    triangle(floor(this.frontNow.x), floor(this.frontNow.y), floor(this.rightNow.x), floor(this.rightNow.y), floor(this.leftNow.x), floor(this.leftNow.y));

    this.positions = append(this.positions, createVector(this.pos.x, this.pos.y));

    if (!this.moved) {
      if (key_up || key_down || key_left || key_right) {
        this.moved = true;
        this.firstAngle = angle(this.speed.x, this.speed.y);
      }
    }


    this.alive++;
  }
  //println(speed.x + ", " + speed.y);
}

Player.prototype.die = function() {
  //timeWarpDraw = new TimeWarpDraw();
  this.dead = true;
  gameRunning = false;
}
