function Enemy() {
  //this.id;
  //this.pos;
  //this.speed;
  //this.maxSpeed;
  this.positions = [];
  this.distances = [];

  //this.front;
  //this.left;
  //this.right;
  //this.frontNow;
  //this.leftNow;
  //this.rightNow;
}

Enemy.prototype.setup = function(id, pos, maxSpeed) {
  this.id = id;
  this.pos = pos;
  this.speed = createVector(0, 0);
  this.maxSpeed = maxSpeed; //random(2.5, 6);

  // gestalt initialisieren
  this.front = createVector(0, 5);
  this.left = createVector(-4, -5);
  this.right = createVector(4, -5);
  this.frontNow = createVector(this.front.x, this.front.y);
  this.leftNow = createVector(this.left.x, this.left.y);
  this.rightNow = createVector(this.right.x, this.right.y);
}

Enemy.prototype.deDraw = function() {
  // vorherige Position mit weiß uebermalen
  if (this.positions.length > 0) rect(this.positions[this.positions.length - 1].x, this.positions[this.positions.length - 1].y, 18, 18);
}

Enemy.prototype.update = function() {
  this.speed.mult(0.99);

  var playerDist = createVector(this.pos.x, this.pos.y);
  playerDist.sub(player.pos);
  if (playerDist.mag() < 5) { // ueberpruefen, ob gegner spieler erwischt hat
    if (killer == -1) killer = this.id;
    if (!godmode) player.die();
  }

  // zum spieler fliegen
  var target = createVector(this.pos.x, this.pos.y);
  target.sub(player.pos);
  target.mult(0.0012);
  this.speed.sub(target);

  // von anderen gegnern wegfliegen
  for (var i = this.id + 1; i < enemies.length; i++) {
    var enemyDist = createVector(enemies[i].pos.x, enemies[i].pos.y);
    enemyDist.sub(this.pos);

    if (enemyDist.mag() < 10) { // wenn zu nah
      enemyDist.mult(0.0115);
      this.speed.sub(enemyDist); // weg vom anderen gegner
      enemyDist.set(-enemyDist.x, -enemyDist.y, 0);
      enemies[i].speed.sub(enemyDist); // anderer gegner weg von aktuellem gegner
    }
  }

  // speed auf maxspeed reduzieren
  this.speed.normalize();
  this.speed.mult(this.maxSpeed);

  this.pos.add(this.speed);

  // gegner zeichnen
  this.frontNow.set(this.front);
  this.frontNow.rotate(angle(this.speed.x, this.speed.y) - HALF_PI);
  this.frontNow.add(this.pos);

  this.leftNow.set(this.left);
  this.leftNow.rotate(angle(this.speed.x, this.speed.y) - HALF_PI);
  this.leftNow.add(this.pos);

  this.rightNow.set(this.right);
  this.rightNow.rotate(angle(this.speed.x, this.speed.y) - HALF_PI);
  this.rightNow.add(this.pos);

  // spawnAnimation starten
  if (this.positions.length == 0) spawnAnimation.setup(this.pos, this.frontNow, this.leftNow, this.rightNow, enemyColor);

  // werte speichern
  this.positions = append(this.positions, createVector(this.pos.x, this.pos.y));
  //distances = append(distances, playerDist.mag());

  triangle(floor(this.frontNow.x), floor(this.frontNow.y), floor(this.rightNow.x), floor(this.rightNow.y), floor(this.leftNow.x), floor(this.leftNow.y));
}
