function keyPressed() {
  if (keyCode === LEFT_ARROW || keyCode == 65) key_left = true;
  if (keyCode === RIGHT_ARROW || keyCode == 68) key_right = true;
  if (keyCode === UP_ARROW || keyCode == 87) key_up = true;
  if (keyCode === DOWN_ARROW || keyCode == 83) key_down = true;
  if (keyCode == 32 || keyCode === ENTER || keyCode === RETURN) key_space = true;
  //return false;
  if (hudActive) hudControl.handleKeyPress(keyCode);
}

function keyReleased() {
  if (keyCode === LEFT_ARROW || keyCode == 65) key_left = false;
  if (keyCode === RIGHT_ARROW || keyCode == 68) key_right = false;
  if (keyCode === UP_ARROW || keyCode == 87) key_up = false;
  if (keyCode === DOWN_ARROW || keyCode == 83) key_down = false;
  if (keyCode == 32 || keyCode === ENTER || keyCode === RETURN) key_space = false;

  //return false;
}
