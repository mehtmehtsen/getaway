<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>

<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
  <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>GETAWAY by Komeht</title>

	<link rel='stylesheet' type='text/css' href='app/css/main.css' />

</head>

<body>
	<div class="hud-wrapper hud-main">

		<div class="hud">
			<div class="hud-logo">
				<img class="logo" src="app/images/logo.svg" alt="GETAWAY" />
			</div>

			<div class="hud-menu hud-menu-main"> <!-- es ist das normale menu -->
				<div class="hud-menu-content">

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link new-game"> <!-- new game -->
							<div class="hud-menu-pt-area">
								new game
							</div>
						</a>
					</div>

					<div class="hud-menu-pt save-graphic-grey">

						<div class="hud-menu-pt-area greyed-out grey">
							save graphic
						</div>

						<a href="javascript:;" class="link not-greyed-out save-graphic invisible" style="display: none;"> <!-- save graphic -->
							<div class="hud-menu-pt-area">
								save graphic
							</div>
						</a>

					</div>

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link how-to-play"> <!-- how to play -->
							<div class="hud-menu-pt-area">
								how to play
							</div>
						</a>
					</div>

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link gallery"> <!-- gallery -->
							<div class="hud-menu-pt-area">
								gallery
							</div>
						</a>
					</div>

				</div>
			</div>

			<div class="hud-menu hud-menu-save" style="display: none;"> <!-- save graphic-menu -->
				<div class="hud-menu-content">

					<div class="hud-menu-pt savetogallery-grey">

						<a href="javascript:;" class="link savetogallery"> <!-- gallery -->
							<div class="hud-menu-pt-area">
								save online
							</div>
						</a>

						<div class="hud-menu-pt-area saving" style="display: none;">
							saving...
						</div>

						<div class="hud-menu-pt-area saved" style="display: none;">
							saved!
						</div>

						<div class="hud-menu-pt-area error" style="display: none;">
							error!1
						</div>

					</div>

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link pdfdownload"> <!-- pdf -->
							<div class="hud-menu-pt-area">
								save pdf
							</div>
						</a>
					</div>

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link imagedownload"> <!-- image -->
							<div class="hud-menu-pt-area">
								save image
							</div>
						</a>
					</div>

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link cancel-save"> <!-- cancel -->
							<div class="hud-menu-pt-area">
								back
							</div>
						</a>
					</div>

				</div>
			</div>

			<div class="hud-menu-howto" style="display: none;"> <!-- how to play menu -->

				<div class="hud-menu-howto-illu">
					<img src="app/images/howtoplayIllu.svg" alt="PLAY BY TOUCHING OR KEYBOARD" />
					<p>
						escape!<br>
						draw something!
					</p>
				</div>

				<div class="hud-menu">
					<div class="hud-menu-content">
						<div class="hud-menu-pt">
							<a href="javascript:;" class="link cancel-howto"> <!-- cancel -->
								<div class="hud-menu-pt-area">
									back
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>

			<div class="hud-menu hud-menu-gallery" style="display: none;"> <!-- gallery menu -->
				<div class="hud-menu-content">

					<div class="hud-menu-pt">
						<a href="javascript:;" class="link cancel-gallery"> <!-- back to game -->
							<div class="hud-menu-pt-area">
								back to game
							</div>
						</a>
					</div>

				</div>
			</div>

			<div class="hud-landscape-social-wrapper">
				<div class="hud-landscape"> <!-- landscape warning -->
					<img src="app/images/landscape.svg" alt="LANDSCAPE MODE RECOMMENDED" />
					landscape mode recommended
				</div>

				<div class="hud-social"> <!-- social buttons -->
					<div class="hud-social-content">
						<div class="shariff" data-theme="standard" data-services="[&quot;facebook&quot;,&quot;twitter&quot;]" data-orientation="horizontal"></div>

						<div class="hud-menu-pt">
							by <a href="http://www.komeht.de" class="link">
					 			<span class="hud-menu-pt-area">
									komeht
								</span>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="hud-gallery-thumb-wrapper" style="display: none;">
				<div class="hud-social-content">
					<div class="gallery-thumb"></div>
				</div>
			</div>

		</div>
	</div>

	<div class="hud-hide-main"> <!-- es ist nur fuer hud-hide -->
		<a href="javascript:;" class="hvr link"> <!-- hide hud -->

			<div class="hud-hide">

				<div class="hud-hide-bg-wrapper">
					<div class="hud-hide-bg"></div>
				</div>

				<div class="hud-hide-wrapper">
					<div class="hvr0">
						<img src="app/images/hidehud.svg" alt="HIDE HUD" />
					</div>
					<div class="hvr1" style="display: none;">
						<img src="app/images/hidehudU.svg" alt="HIDE HUD" />
					</div>
				</div>

			</div>

		</a>
	</div>

	<div id="getaway">
		<div class="getaway-fade-out"></div>
		<div class="getaway-move" style="display: none;">move!</div>
	</div>

	<div id="gallery" style="display: none;">
		<div class="gallery-grid"> </div>
		<div class="btn-wrapper">
			<div class="load-more-btn btn">
				<div class="load-more">load more</div>
				<div class="loading" style="display: none;">loading...</div>
				<div class="loading-error blinking-red" style="display: none;">error!1</div>
			</div>
		</div>
	</div>

	<!-- PHOTOSWIPE -->
	<!-- Root element of PhotoSwipe. Must have class pswp. -->
	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
    <!-- Background of PhotoSwipe.
		It's a separate element as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">
      <!-- Container that holds slides.
      PhotoSwipe keeps only 3 of them in the DOM to save memory.
      Don't modify these 3 pswp__item elements, data is added later on. -->
      <div class="pswp__container">
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
        <div class="pswp__item"></div>
      </div>

      <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
      <div class="pswp__ui pswp__ui--hidden">
        <div class="pswp__top-bar">
          <!--  Controls are self-explanatory. Order can be changed. -->

          <div class="pswp__counter"></div>

          <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
          <button class="pswp__button pswp__button--share" title="Share"></button>
          <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
          <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

          <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
          <!-- element will get class pswp__preloader--active when preloader is running -->
          <div class="pswp__preloader">
            <div class="pswp__preloader__icn">
              <div class="pswp__preloader__cut">
                <div class="pswp__preloader__donut"></div>
              </div>
            </div>
          </div>
        </div>

      	<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
          <div class="pswp__share-tooltip"></div>
        </div>

        <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
        </button>

        <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
        </button>

        <div class="pswp__caption">
          <div class="pswp__caption__center"></div>
        </div>
      </div>
    </div>
	</div>
	<!-- /PHOTOSWIPE -->

  <script type='text/javascript' src="app/js/main.js"></script>

</body>
</html>
