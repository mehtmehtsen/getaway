sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'

sudo apt-get update
sudo apt-get install -y apache2
sudo apt-get install -y php5
sudo apt-get install -y mysql-server php5-mysql
sudo service apache2 reload

sudo rm -rf /var/www/html
sudo ln -fs /vagrant /var/www/html
